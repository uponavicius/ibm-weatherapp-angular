import {Injectable} from '@angular/core';
import {HttpService} from './http.service';

@Injectable({
  providedIn: 'root'
})
export class DateService {
  constructor() {
  }

  today() {
    return new Date().toISOString().slice(0, 10);
  }

}
