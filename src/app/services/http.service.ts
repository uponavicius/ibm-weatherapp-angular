import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {WeatherDataModel} from '../models/weather-data.model';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  url = 'http://localhost:8080/';
  weatherUri = 'weather';

  constructor(private http: HttpClient) {
  }

  getWeatherData(date): Observable<WeatherDataModel[]> {
    return this.http.get<WeatherDataModel[]>(this.url + this.weatherUri, {params: new HttpParams().set('date', date)});
  }

  getMinDate(): Observable<string[]> {
    return this.http.get<string[]>(this.url + 'minDate');
  }
}
