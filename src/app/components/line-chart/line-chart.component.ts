import {Component, OnInit} from '@angular/core';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {HttpService} from '../../services/http.service';
import {ChartDataSets} from 'chart.js';
import {Color, Label} from 'ng2-charts';
import {WeatherDataModel} from '../../models/weather-data.model';
import {DateService} from '../../services/date.service';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements OnInit {
  // https://material.angular.io/components/datepicker/overview

  weatherData: WeatherDataModel[];
  allTemp: number[] = [];
  minDate: any;
  maxDate: any = this.dateService.today();
  tempMin: number;
  tempMax: number;
  tempAvg: number;

  lineChartData: ChartDataSets[] = [
    {data: [0], label: 'Temperature C°'}
  ];

  lineChartLabels: Label[] = []; // months

  lineChartOptions = {
    responsive: true,
    scales: {
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'C°',
        }
      }],
      xAxes: [{
        gridLines: {
          display: false
        },
        scaleLabel: {
          display: true,
          labelString: 'Time',
        }
      }]
    }
  };

  lineChartColors: Color[] = [
    {
      borderColor: '#ff4444',
      backgroundColor: 'rgba(255, 68, 68,0.28)',
    }
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';


  constructor(private httpService: HttpService,
              private dateService: DateService) {
  }

  ngOnInit(): void {
    this.getMinDate();
  }


  EndDateChange(date: any): void {
    this.httpService.getWeatherData(date).subscribe(resp => {
      if (resp !== null) {
        this.weatherData = resp;
        this.allTemp = [];
        this.lineChartLabels = [];
        let sum = 0;
        for (let i = 0; i < resp.length; i++) {
          this.allTemp.push(+this.weatherData[i].tempValue);
          this.lineChartLabels.push(this.weatherData[i].observationTimeValue.toString().substr(11, 5));
          sum += this.weatherData[i].tempValue;
        }
        this.tempAvg = Math.round((sum / resp.length) * 100) / 100;
        this.tempMin = Math.min.apply(null, this.allTemp);
        this.tempMax = Math.max.apply(null, this.allTemp);
        this.lineChartData = [
          {data: this.allTemp, label: 'Temperature C°'}
        ];

      }

    });
  }

  getMinDate() {
    this.httpService.getMinDate().subscribe(resp => {
      this.minDate = new Date(resp.toString());
    });
  }


}
