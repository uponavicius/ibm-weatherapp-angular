export interface WeatherDataModel {
  id: number;
  tempValue: number;
  tempUnits: string;
  observationTimeValue: Date;
}
