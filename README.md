# IbmWeatherAppAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.0.

#### To start project:
* Clone repository https://gitlab.com/uponavicius/ibm-weatherapp-angular.git
* Open command prompt and go  to root project folder .
* Run command `npm install`.
* Run command `ng serve` for a dev server.
* Navigate to `http://localhost:4200/`.


ATTENTION! First run this project - https://gitlab.com/uponavicius/ibm-weatherapp-spring

